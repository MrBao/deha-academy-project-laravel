<?php

namespace Tests;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, WithFaker;

    protected $userRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = app()->make(UserRepository::class);
    }

    public function loginAsAdmin()
    {
        return $this->actingAs($this->userRepository->getAdminUser());
    }

    public function loginUserWithoutPermission()
    {
        return $this->actingAs($this->createUser());
    }

    public function createUser()
    {
        return User::factory()->create();
    }

    public function logout()
    {
        Auth::logout();
    }
}
