<?php

namespace Tests\Feature\users;

use Illuminate\Http\Response;
use Tests\TestCase;

class GetListUserTest extends TestCase
{
    /** @test */
    public function admin_can_get_list_user()
    {
        $this->loginAsAdmin();
        $user = $this->createUser();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs($this->getView());
        $response->assertSee($user->name);
    }

    /** @test */
    public function can_not_get_list_user_if_unauthenticated()
    {
        $this->logout();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_not_get_list_user_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function getRoute()
    {
        return route('users.index');
    }

    public function getView()
    {
        return 'users.index';
    }
}
