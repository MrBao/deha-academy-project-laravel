<?php

namespace Tests\Feature\users;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    /** @test */
    public function user_can_be_created_by_admin()
    {
        $this->loginAsAdmin();
        $user = $this->makeData();
        $userCount = User::count();
        $response = $this->post($this->getRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getIndexRoute());
        $response->assertSessionHas('message', 'create'.$user['name'].'success');
        $this->assertEquals($userCount + 1, User::count());
        $this->assertDatabaseHas($this->getTableName(), [
            'name' => $user['name'],
            'email' => $user['email'],
            'role_id' => $user['role_id']
        ]);
    }

    /** @test */
    public function create_user_requires_validation()
    {
        $this->loginAsAdmin();
        $data = $this->makeData();
        $data['name'] = '';
        $response = $this->from($this->getCreateViewsRoute())->post($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('name', 'The name field is required.')
            ->assertRedirect($this->getCreateViewsRoute());
    }

    /** @test */
    public function can_not_create_user_if_unauthenticated()
    {
        $this->logout();
        $data = $this->makeData();
        $response = $this->post($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_not_create_user_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->makeData();
        $response = $this->post($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function makeData()
    {
        $data = User::factory()->make()->toArray();
        $data['password'] = $data['confirm-password'] = $this->faker->password;
        return $data;
    }

    public function getRoute()
    {
        return route('users.store');
    }

    public function getCreateViewsRoute()
    {
        return route('users.create');
    }

    public function getIndexRoute()
    {
        return route('users.index');
    }

    public function getTableName()
    {
        return 'users';
    }
}
