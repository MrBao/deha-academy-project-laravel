<?php

namespace Tests\Feature\users;

use Illuminate\Http\Response;
use Tests\TestCase;

class GetUserTest extends TestCase
{
    /** @test */
    public function admin_can_get_user()
    {
        $this->loginAsAdmin();
        $data = $this->createUser();
        $response = $this->get($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs($this->getView());
        $response->assertSee($data->name);
    }

    /** @test */
    public function can_not_get_user_if_unauthenticated()
    {
        $this->logout();
        $data = $this->createUser();
        $response = $this->get($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function can_not_get_if_user_not_exist()
    {
        $this->loginAsAdmin();
        $response = $this->get($this->getRoute(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function user_can_not_get_user_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->createUser();
        $response = $this->get($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function getRoute($id)
    {
        return route('users.show', $id);
    }

    public function getView()
    {
        return 'users.show';
    }
}
