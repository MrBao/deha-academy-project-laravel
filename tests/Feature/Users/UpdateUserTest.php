<?php

namespace Tests\Feature\users;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    /** @test */
    public function admin_can_be_updated_user()
    {
        $this->loginAsAdmin();
        $data = $this->createUser();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getIndexRoute());
        $response->assertSessionHas('message', 'update'.$dataUpdate['name'].'success');
        $this->assertDatabaseHas($this->getTableName(), [
            'id' => $data['id'],
            'name' => $dataUpdate['name'],
            'email' => $dataUpdate['email'],
            'role_id' => $dataUpdate['role_id']
        ]);
    }

    /** @test */
    public function update_user_requires_validation()
    {
        $this->loginAsAdmin();
        $data = $this->createUser();
        $dataUpdate = $this->makeData();
        $dataUpdate['name'] = '';
        $response = $this->from($this->getEditUserRoute($data->id))->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('name', 'The name field is required.')
            ->assertRedirect($this->getEditUserRoute($data->id));
    }

    /** @test */
    public function can_not_update_user_if_unauthenticated()
    {
        $this->logout();
        $data = $this->createUser();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function can_not_update_if_user_not_exist()
    {
        $this->loginAsAdmin();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute(-1), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function user_can_not_update_user_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->createUser();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function makeData()
    {
        $data = User::factory()->make()->toArray();
        $data['password'] = $data['confirm-password'] = $this->faker->password;
        unset($data['email_verified_at']);
        return $data;
    }

    public function getRoute($id)
    {
        return route('users.update', $id);
    }

    public function getEditUserRoute($id)
    {
        return route('users.edit', $id);
    }

    public function getIndexRoute()
    {
        return route('users.index');
    }

    public function getTableName()
    {
        return 'users';
    }
}
