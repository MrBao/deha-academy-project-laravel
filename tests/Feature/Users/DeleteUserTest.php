<?php

namespace Tests\Feature\users;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    /** @test */
    public function admin_can_be_deleted_user()
    {
        $this->loginAsAdmin();
        $data = $this->createUser();
        $dataCount = $this->getDataCount();
        $response = $this->delete($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getIndexRoute());
        $response->assertSessionHas('message', 'delete success');
        $this->assertEquals($dataCount - 1, $this->getDataCount());
        $this->assertDatabaseMissing('users', ['id' => $data->id]);
    }

    /** @test */
    public function can_not_delete_user_if_unauthenticated()
    {
        $this->logout();
        $data = $this->createUser();
        $response = $this->delete($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_not_delete_user_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->createUser();
        $response = $this->delete($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function getRoute($id)
    {
        return route('users.destroy', $id);
    }

    public function getIndexRoute()
    {
        return route('users.index');
    }

    public function getDataCount()
    {
        return User::count();
    }
}
