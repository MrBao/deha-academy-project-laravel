<?php

namespace Tests\Feature;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetRoleTest extends TestCase
{
    /** @test */
    public function admin_can_get_role()
    {
        $this->loginAsAdmin();
        $data = $this->createData();
        $response = $this->get($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs($this->getView());
        $response->assertSee($data->name);
    }

    /** @test */
    public function can_not_get_role_if_authenticated()
    {
        $this->logout();
        $data = $this->createData();
        $response = $this->get($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function can_not_get_if_role_not_exist()
    {
        $this->loginAsAdmin();
        $response = $this->get($this->getRoute(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function user_can_not_get_role_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->createData();
        $response = $this->get($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function getRoute($id)
    {
        return route('roles.show', $id);
    }

    public function getView()
    {
        return 'roles.show';
    }

    public function createData()
    {
        return Role::factory()->create();
    }
}
