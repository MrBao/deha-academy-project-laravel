<?php

namespace Tests\Feature;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{
    /** @test */
    public function admin_can_be_created_role()
    {
        $this->loginAsAdmin();
        $data = $this->makeData();
        $response = $this->post($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getIndexRoute());
        $response->assertSessionHas('message', 'create'.$data['name'].'success');
        $this->assertDatabaseHas($this->getTableName(), $data);
    }

    /** @test */
    public function create_role_requires_validation()
    {
        $this->loginAsAdmin();
        $data = $this->makeData();
        $data['name'] = '';
        $response = $this->from($this->getCreateViewRoute())->post($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('name', 'The name field is required.')
            ->assertRedirect($this->getCreateViewRoute());
    }

    /** @test */
    public function can_not_create_role_if_unauthenticated()
    {
        $this->logout();
        $data = $this->makeData();
        $response = $this->post($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_not_create_role_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->makeData();
        $response = $this->post($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function makeData()
    {
        return Role::factory()->make()->toArray();
    }

    public function getRoute()
    {
        return route('roles.store');
    }

    public function getIndexRoute()
    {
        return route('roles.index');
    }

    public function getCreateViewRoute()
    {
        return route('roles.create');
    }

    public function getTableName()
    {
        return 'roles';
    }
}
