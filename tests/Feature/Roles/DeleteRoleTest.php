<?php

namespace Tests\Feature;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{
    /** @test */
    public function admin_can_be_deleted_role()
    {
        $this->loginAsAdmin();
        $data = $this->createData();
        $dataCount = $this->getDataCount();
        $response = $this->delete($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getIndexRoute());
        $response->assertSessionHas('message', 'delete success');
        $this->assertEquals($dataCount - 1, $this->getDataCount());
        $this->assertDatabaseMissing('roles', $data->toArray());
    }

    /** @test */
    public function can_not_delete_role_if_unauthenticated()
    {
        $this->logout();
        $data = $this->createData();
        $response = $this->delete($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_not_delete_role_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->createData();
        $response = $this->delete($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function createData()
    {
        return Role::factory()->create();
    }

    public function getRoute($id)
    {
        return route('roles.destroy', $id);
    }

    public function getIndexRoute()
    {
        return route('roles.index');
    }

    public function getDataCount()
    {
        return Role::count();
    }
}
