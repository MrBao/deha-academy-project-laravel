<?php

namespace Tests\Feature;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateRoleTest extends TestCase
{
    /** @test */
    public function admin_can_be_updated_role()
    {
        $this->loginAsAdmin();
        $data = $this->createData();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getIndexRoute());
        $response->assertSessionHas('message', 'update'.$dataUpdate['name'].'success');
        $this->assertDatabaseHas($this->getTableName(), $dataUpdate);
    }

    /** @test */
    public function update_role_requires_validation()
    {
        $this->loginAsAdmin();
        $data = $this->createData();
        $dataUpdate = $this->makeData();
        $dataUpdate['name'] = '';
        $response = $this->from($this->getEditViewRoute($data->id))->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('name', 'The name field is required.')
            ->assertRedirect($this->getEditViewRoute($data->id));
    }

    /** @test */
    public function can_not_update_role_if_unauthenticated()
    {
        $this->logout();
        $data = $this->createData();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function can_not_update_if_role_not_exist()
    {
        $this->loginAsAdmin();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute(-1), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function user_can_not_update_role_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->createData();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function createData()
    {
        return Role::factory()->create();
    }

    public function makeData()
    {
        return Role::factory()->make()->toArray();
    }

    public function getEditViewRoute($id)
    {
        return route('roles.edit', $id);
    }

    public function getRoute($id)
    {
        return route('roles.update', $id);
    }

    public function getIndexRoute()
    {
        return route('roles.index');
    }

    public function getTableName()
    {
        return 'roles';
    }
}
