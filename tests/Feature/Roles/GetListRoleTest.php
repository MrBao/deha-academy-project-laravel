<?php

namespace Tests\Feature\roles;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListRoleTest extends TestCase
{
    /** @test */
    public function admin_can_get_list_role()
    {
        $this->loginAsAdmin();
        $role = $this->createRole();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs($this->getView());
        $response->assertSee($role->name);
    }

    /** @test */
    public function can_not_get_list_role_if_unauthenticated()
    {
        $this->logout();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_not_get_list_role_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function getRoute()
    {
        return route('roles.index');
    }

    public function getView()
    {
        return 'roles.index';
    }

    public function createRole()
    {
        return Role::factory()->create();
    }
}
