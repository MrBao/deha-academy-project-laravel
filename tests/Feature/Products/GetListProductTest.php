<?php

namespace Tests\Feature\products;

use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    /** @test */
    public function admin_can_get_list_product()
    {
        $this->loginAsAdmin();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs($this->getView());
    }

    /** @test */
    public function can_not_get_list_product_if_unauthenticated()
    {
        $this->logout();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_get_list_data_product_with_api()
    {
        $product = $this->createProduct();
        $response = $this->getJson($this->getListApiRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn (AssertableJson $json) => $json
            ->has('data', fn (AssertableJson $json) => $json
                ->has('pagination')
                ->has('data', fn (AssertableJson $json) => $json
                    ->first(fn (AssertableJson $json) => $json
                        ->where('id', $product->id)
                        ->where('name', $product->name)
                        ->where('category_id', $product->category_id)
                        ->where('price', $product->price)
                        ->etc())->etc()))
            ->where('message', Response::$statusTexts[Response::HTTP_OK]));
    }

    /** @test */
    public function user_can_not_get_list_product_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function getRoute()
    {
        return route('products.index');
    }

    public function getView()
    {
        return 'products.index';
    }

    public function getListApiRoute()
    {
        return route('api-products.list');
    }

    public function createProduct()
    {
        return Product::factory()->create();
    }
}
