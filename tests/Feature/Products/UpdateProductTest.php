<?php

namespace Tests\Feature\products;

use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    /** @test */
    public function admin_can_be_updated_product()
    {
        $this->loginAsAdmin();
        $product = $this->createData();
        $dataUpdate = $this->makeData();
        $dataUpdate['image'] = $this->makeImage();
        $response = $this->putJson($this->getRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) => $json
            ->has('data', fn (AssertableJson $json) => $json
                ->where('id', $product['id'])
                ->where('name', $dataUpdate['name'])
                ->where('category_id', $dataUpdate['category_id'])
                ->where('price', $dataUpdate['price'])
                ->etc())
            ->where('message', 'OK'));
        $dataUpdate['id'] = $product['id'];
        unset($dataUpdate['image']);
        $this->assertDatabaseHas($this->getTableName(), $dataUpdate);
    }

    /** @test */
    public function update_product_requires_validation()
    {
        $this->loginAsAdmin();
        $product = $this->createData();
        $dataUpdate = $this->makeData();
        $dataUpdate['name'] = $dataUpdate['price'] = '';
        $response = $this->putJson($this->getRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn (AssertableJson $json) => $json
            ->has('errors', fn (AssertableJson $json) => $json
                ->has('name')
                ->has('price')->etc())
            ->where('message', 'The given data was invalid.'));
    }

    /** @test */
    public function can_not_update_product_if_unauthenticated()
    {
        $this->logout();
        $data = $this->createData();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function can_not_update_if_product_not_exist()
    {
        $this->loginAsAdmin();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute(-1), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function user_can_not_update_product_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->createData();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function createData()
    {
        return Product::factory()->create();
    }

    public function makeData()
    {
        return Product::factory()->make()->toArray();
    }

    public function getRoute($id)
    {
        return route('products.update', $id);
    }

    public function getIndexRoute()
    {
        return route('products.index');
    }

    public function getTableName()
    {
        return 'products';
    }

    public function makeImage()
    {
        return UploadedFile::fake()->image(time().'.png');
    }
}
