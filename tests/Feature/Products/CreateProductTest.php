<?php

namespace Tests\Feature\products;

use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    /** @test */
    public function admin_can_be_created_product()
    {
        $this->loginAsAdmin();
        $product = $this->makeData();
        $product['image'] = $this->makeImage();
        $response = $this->postJson($this->getRoute(), $product);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) => $json
            ->has('data', fn (AssertableJson $json) => $json
                ->where('name', $product['name'])
                ->where('category_id', $product['category_id'])
                ->where('price', $product['price'])
                ->etc())
            ->where('message', 'OK'));
        unset($product['image']);
        $this->assertDatabaseHas($this->getTableName(), $product);
    }

    /** @test */
    public function create_product_requires_validation()
    {
        $this->loginAsAdmin();
        $product = Product::factory()->make(['name' => '', 'price' => ''])->toArray();
        $response = $this->postJson($this->getRoute(), $product);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn (AssertableJson $json) => $json
            ->has('errors', fn (AssertableJson $json) => $json
                ->has('name')
                ->has('price')->etc())
            ->where('message', 'The given data was invalid.'));
    }

    /** @test */
    public function can_not_create_product_if_unauthenticated()
    {
        $this->logout();
        $data = $this->makeData();
        $response = $this->post($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_not_create_product_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->makeData();
        $response = $this->post($this->getRoute(), $data);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function makeData()
    {
        return Product::factory()->make()->toArray();
    }

    public function getRoute()
    {
        return route('products.store');
    }

    public function getIndexRoute()
    {
        return route('products.index');
    }

    public function getTableName()
    {
        return 'products';
    }

    public function makeImage()
    {
        return UploadedFile::fake()->image(time().'.png');
    }
}
