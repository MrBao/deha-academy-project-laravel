<?php

namespace Tests\Feature\products;

use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    /** @test */
    public function admin_can_be_deleted_product()
    {
        $this->loginAsAdmin();
        $product = $this->createData();
        $response = $this->deleteJson($this->getRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) => $json
            ->has('data', fn (AssertableJson $json) => $json
                ->where('id', $product['id'])->etc())
            ->where('message', 'OK'));
        $this->assertDatabaseMissing('products', ['id' => $product->id]);
    }

    /** @test */
    public function can_not_delete_product_if_unauthenticated()
    {
        $this->logout();
        $data = $this->createData();
        $response = $this->delete($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_not_delete_product_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->createData();
        $response = $this->delete($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function createData()
    {
        return Product::factory()->create();
    }

    public function getRoute($id)
    {
        return route('products.destroy', $id);
    }

    public function getIndexRoute()
    {
        return route('products.index');
    }

    public function getDataCount()
    {
        return Product::count();
    }
}
