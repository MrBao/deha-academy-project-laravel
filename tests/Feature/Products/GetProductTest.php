<?php

namespace Tests\Feature\products;

use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetProductTest extends TestCase
{
    /** @test */
    public function user_can_get_product()
    {
        $product = $this->createProduct();
        $response = $this->getJson($this->getProductRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) => $json
            ->has('data', fn (AssertableJson $json) => $json
                ->where('id', $product->id)
                ->where('name', $product->name)
                ->where('category_id', $product->category_id)
                ->where('price', $product->price)
                ->etc())
            ->where('message', 'OK'));
    }

    /** @test */
    public function can_not_get_if_product_not_exist()
    {
        $this->loginAsAdmin();
        $response = $this->getJson($this->getProductRoute(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function getProductRoute($id)
    {
        return route('api-products.get', $id);
    }

    public function createProduct()
    {
        return Product::factory()->create();
    }
}
