<?php

namespace Tests\Feature\categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetCategoryTest extends TestCase
{
    /** @test */
    public function admin_can_get_category()
    {
        $this->loginAsAdmin();
        $data = $this->createData();
        $response = $this->get($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs($this->getView());
        $response->assertSee($data->name);
    }

    /** @test */
    public function can_not_get_category_if_unauthenticated()
    {
        $this->logout();
        $data = $this->createData();
        $response = $this->get($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function can_not_get_if_category_not_exist()
    {
        $this->loginAsAdmin();
        $response = $this->get($this->getRoute(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function user_can_not_get_category_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->createData();
        $response = $this->get($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function getRoute($id)
    {
        return route('categories.show', $id);
    }

    public function getView()
    {
        return 'categories.show';
    }

    public function createData()
    {
        return Category::factory()->create();
    }
}
