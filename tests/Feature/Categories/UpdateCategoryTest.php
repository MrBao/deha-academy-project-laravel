<?php

namespace Tests\Feature\categories;

use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    /** @test */
    public function admin_can_be_updated_category()
    {
        $this->loginAsAdmin();
        $data = $this->createData();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getIndexRoute());
        $response->assertSessionHas('message', 'update'.$dataUpdate['name'].'success');
        $this->assertDatabaseHas($this->getTableName(), $dataUpdate);
    }

    /** @test */
    public function update_category_requires_validation()
    {
        $this->loginAsAdmin();
        $data = $this->createData();
        $dataUpdate = $this->makeData();
        $dataUpdate['name'] = '';
        $response = $this->from($this->getEditViewRoute($data->id))->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('name', 'The name field is required.')
            ->assertRedirect($this->getEditViewRoute($data->id));
    }

    /** @test */
    public function can_not_update_category_if_unauthenticated()
    {
        $this->logout();
        $data = $this->createData();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function can_not_update_if_category_not_exist()
    {
        $this->loginAsAdmin();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute(-1), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function user_can_not_update_category_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->createData();
        $dataUpdate = $this->makeData();
        $response = $this->put($this->getRoute($data->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function createData()
    {
        return Category::factory()->create();
    }

    public function makeData()
    {
        return Category::factory()->make()->toArray();
    }

    public function getEditViewRoute($id)
    {
        return route('categories.edit', $id);
    }

    public function getRoute($id)
    {
        return route('categories.update', $id);
    }

    public function getIndexRoute()
    {
        return route('categories.index');
    }

    public function getTableName()
    {
        return 'categories';
    }
}
