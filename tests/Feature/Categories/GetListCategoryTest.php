<?php

namespace Tests\Feature\categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{
    /** @test */
    public function admin_can_get_list_category()
    {
        $this->loginAsAdmin();
        $category = $this->createCategory();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs($this->getView());
        $response->assertSee($category->name);
    }

    /** @test */
    public function can_not_get_list_category_if_unauthenticated()
    {
        $this->logout();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_not_get_list_category_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $response = $this->get($this->getRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function getRoute()
    {
        return route('categories.index');
    }

    public function getView()
    {
        return 'categories.index';
    }

    public function createCategory()
    {
        return Category::factory()->create();
    }
}
