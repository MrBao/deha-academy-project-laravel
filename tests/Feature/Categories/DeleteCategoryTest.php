<?php

namespace Tests\Feature\categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    /** @test */
    public function admin_can_be_deleted_category()
    {
        $this->loginAsAdmin();
        $data = $this->createData();
        $dataCount = $this->getDataCount();
        $response = $this->delete($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getIndexRoute());
        $response->assertSessionHas('message', 'delete success');
        $this->assertEquals($dataCount - 1, $this->getDataCount());
        $this->assertDatabaseMissing('categories', ['id' => $data->id]);
    }

    /** @test */
    public function can_not_delete_category_if_unauthenticated()
    {
        $this->logout();
        $data = $this->createData();
        $response = $this->delete($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_not_delete_category_if_has_not_permission()
    {
        $this->loginUserWithoutPermission();
        $data = $this->createData();
        $response = $this->delete($this->getRoute($data->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $response->assertForbidden();
    }

    public function createData()
    {
        return Category::factory()->create();
    }

    public function getRoute($id)
    {
        return route('categories.destroy', $id);
    }

    public function getIndexRoute()
    {
        return route('categories.index');
    }

    public function getDataCount()
    {
        return Category::count();
    }
}
