<?php

namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function all()
    {
        return $this->categoryRepository->all();
    }

    public function paginate()
    {
        return $this->categoryRepository->paginate(5);
    }

    public function findOrFail($id)
    {
        return $this->categoryRepository->findOrFail($id);
    }

    public function create($request)
    {
        return $this->categoryRepository->create($request->all());
    }

    public function update($request, $id)
    {
        return $this->categoryRepository->update($request->all(), $id);
    }

    public function delete($id)
    {
        return $this->categoryRepository->delete($id);
    }
}
