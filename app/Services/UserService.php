<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? '';
        $dataSearch['role_id'] = $request->role_id ?? '';
        return $this->userRepository->search($dataSearch);
    }

    public function findOrFail($id)
    {
        return $this->userRepository->findOrFail($id);
    }

    public function create($request)
    {
        $request = $request->except('confirm-password');
        $request['password'] = Hash::make($request['password']);
        return $this->userRepository->create($request);
    }

    public function update($request, $id)
    {
        $request = $request->except('confirm-password');
        $request['password'] = Hash::make($request['password']);
        return $this->userRepository->update($request, $id);
    }

    public function delete($id)
    {
        return $this->userRepository->delete($id);
    }
}
