<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getFields(array $fields)
    {
        return $this->roleRepository->getFields($fields);
    }

    public function paginate()
    {
        return $this->roleRepository->paginate(5);
    }

    public function findOrFail($id)
    {
        return $this->roleRepository->findOrFail($id);
    }

    public function create($request)
    {
        $role = $this->roleRepository->create($request->except('permissions'));
        $permissions = $request->input('permissions');
        if ($permissions) {
            $role->permission()->attach($permissions);
        }
        return true;
    }

    public function update($request, $id)
    {
        $role = $this->roleRepository->findOrFail($id);
        $this->roleRepository->update($request->except('permissions'), $id);
        $role->permission()->sync($request->input('permissions'));
        return true;
    }

    public function delete($id)
    {
        $role = $this->roleRepository->findOrFail($id);
        if ($this->roleRepository->delete($id)) {
            $role->permission()->detach();
        };
        return true;
    }
}
