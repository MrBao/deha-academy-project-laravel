<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use App\Traits\HandleImage;

class ProductService
{
    use HandleImage;

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? '';
        $dataSearch['price'] = $request->price ?? '';
        $dataSearch['category'] = $request->category ?? '';
        return $this->productRepository->search($dataSearch);
    }

    public function find($id)
    {
        return $this->productRepository->find($id);
    }

    public function findOrFail($id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function create($request)
    {
        $data = $request->all();
        $imageName = time() . '.' . $data['image']->getClientOriginalExtension();
        $this->saveImage($data['image'], $imageName);
        $data['image'] = $imageName;
        $data['user_id'] = \Auth::user()->id;
        return $this->productRepository->create($data);
    }

    public function update($request, $id)
    {
        $data = $request->except('image');
        $imageFile = $request->file('image');
        $product = $this->productRepository->findOrFail($id);
        if ($imageFile) {
            if (file_exists($product->image_path_origin)) {
                $this->updateImage($imageFile, $product->image_path_origin);
            } else {
                $data['image'] = time() . '.' . $imageFile->getClientOriginalExtension();
                $this->saveImage($imageFile, $data['image']);
            }
        }
        return $this->productRepository->update($data, $id);
    }

    public function delete($id)
    {
        $product = $this->productRepository->findOrFail($id);
        $this->productRepository->delete($id);
        return $this->deleteImage($product->image_path_origin);
    }
}
