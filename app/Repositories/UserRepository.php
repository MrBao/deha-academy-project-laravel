<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{

    public function model()
    {
        return User::class;
    }

    public function search($dataSearch)
    {
        return $this->model->with('role')
            ->orName($dataSearch['name'])
            ->orEmail($dataSearch['name'])
            ->withRole($dataSearch['role_id'])
            ->latest('id')
            ->paginate(5);
    }

    public function getAdminUser()
    {
        return $this->model->withRoleName(config('role.admin'))->first();
    }
}
