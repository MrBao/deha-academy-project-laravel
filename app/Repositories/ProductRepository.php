<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{

    public function model()
    {
        return Product::class;
    }

    public function search($dataSearch)
    {
        return $this->model
            ->with('category')
            ->withName($dataSearch['name'])
            ->withPrice($dataSearch['price'])
            ->withCategory($dataSearch['category'])
            ->latest('id')->paginate(5);
    }
}
