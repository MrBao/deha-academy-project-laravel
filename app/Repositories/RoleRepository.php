<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository extends BaseRepository
{

    public function model()
    {
        return Role::class;
    }

    public function findOrFail($id)
    {
        return $this->model->with('permission')->findOrFail($id);
    }

    public function paginate($row)
    {
        return $this->model->latest('id')->paginate($row);
    }

    public function getSuperUserRole()
    {
        return $this->model->withName(config('role.super_user'))->first();
    }

    public function getAdminRole()
    {
        return $this->model->withName(config('role.admin'))->first();
    }
}
