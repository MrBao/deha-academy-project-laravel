<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository
{

    public function model()
    {
        return Category::class;
    }

    public function paginate($row)
    {
        return $this->model->latest('id')->paginate($row);
    }
}
