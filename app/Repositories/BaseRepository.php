<?php

namespace App\Repositories;

use Illuminate\Contracts\Cache\Repository as Cache;

abstract class BaseRepository
{
    public $model;

    public function __construct()
    {
        $this->makeModel();
    }

    abstract public function model();

    public function makeModel()
    {
        $this->model = app()->make($this->model());
    }

    public function all()
    {
        return $this->model->all();
    }

    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create($input)
    {
        return $this->model->create($input);
    }

    public function update($input, $id)
    {
        return $this->model->findOrFail($id)->update($input);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function paginate($row)
    {
        return $this->model->paginate($row);
    }

    public function getFields(array $fields)
    {
        return $this->model->all($fields);
    }

    public function pluck($firstField, $secondField = null)
    {
        return $this->model->pluck($firstField, $secondField);
    }
}
