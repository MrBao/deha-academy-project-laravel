<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\WithFaker;

class Permission extends Model
{
    use HasFactory, WithFaker;
    protected $table = 'permissions';
    protected $fillable = [
        'name'
    ];

    public function role()
    {
        $this->belongsToMany(Role::class);
    }
}
