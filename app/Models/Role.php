<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';
    protected $fillable = [
        'name'
    ];

    public function permission()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function hasPermission($permissionName)
    {
        return $this->permission->pluck('name')->contains($permissionName);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', $name) : null;
    }
}
