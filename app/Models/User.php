<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function scopeOrName($query, $name)
    {
        return $name ? $query->orWhere('name', 'like', '%'.$name.'%') : null;
    }

    public function scopeOrEmail($query, $name)
    {
        return $name ? $query->orWhere('email', 'like', '%'.$name.'%') : null;
    }

    public function scopeWithRole($query, $roleId)
    {
        return $roleId ? $query->where('role_id', $roleId) : null;
    }

    public function scopeWithRoleName($query, $roleName)
    {
        return $roleName ? $query->whereHas('role', function (Builder $query) use ($roleName) {
            $query->where('name', $roleName);
        }) : null;
    }

    public function isSuperUser()
    {
        return $this->hasRole(config('role.super_user'));
    }

    public function hasPermission($permissionName)
    {
        if ($this->isSuperUser()) {
            return true;
        }

        return $this->role->hasPermission($permissionName);
    }

    public function hasRole($roleName)
    {
        return $this->role()->where('name', $roleName)->exists();
    }
}
