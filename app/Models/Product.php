<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
    protected $fillable = [
        'name',
        'category_id',
        'image',
        'user_id',
        'price'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->Where('name', 'like', '%'.$name.'%') : null;
    }

    public function scopeWithPrice($query, $price)
    {
        return $price ? $query->Where('price', 'like', '%'.$price.'%') : null;
    }

    public function scopeWithCategory($query, $category)
    {
        return $category ? $query->whereHas('category', function (Builder $query) use ($category) {
            $query->where('name', 'like', '%'.$category.'%');
        }) : null;
    }

    public function getPathOfImageAttribute()
    {
        return asset('storage/images/'.$this->image);
    }

    public function getImagePathOriginAttribute()
    {
        return storage_path('app/public/images/'.$this->image);
    }
}
