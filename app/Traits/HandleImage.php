<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile as File;
use Intervention\Image\Facades\Image;

trait HandleImage
{

    public function saveImage(File $image, string $name)
    {
        return $this->handle($image)->save($this->getPathUploads() . $name);
    }

    public function updateImage(File $newImage, string $pathOldImage)
    {
        return $this->handle($newImage)->save($pathOldImage);
    }

    public function deleteImage(string $imagePath)
    {
        return file_exists($imagePath) ? unlink($imagePath) : false;
    }

    public function handle(File $image)
    {
        return Image::make($image)->resize(300, 300);
    }

    public function getPathUploads()
    {
        $pathUpload = storage_path('app/public/images/');
        if (!is_dir($pathUpload)) {
            mkdir($pathUpload);
        }
        return $pathUpload;
    }
}
