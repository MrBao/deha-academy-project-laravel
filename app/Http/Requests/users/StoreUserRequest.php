<?php

namespace App\Http\Requests\users;

use App\Rules\EmailRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => ['required', 'email', new EmailRule],
            'password' => 'required',
            'confirm-password' => 'required|same:password'
        ];
    }
}
