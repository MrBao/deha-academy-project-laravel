<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $role, $permission)
    {
        $user = Auth::user();
        if ($user->role)
        {
            if ($user->hasRole($role) || $user->hasPermission($permission)){
                return $next($request);
            }
        }
        return abort(Response::HTTP_FORBIDDEN)->withErrors([
            'warning' => 'This action is unauthorized.',
        ]);
    }
}
