<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\Response;

class CacheResponseMiddleware
{
    private $cacheTime;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $cacheTime = 600)
    {
        $this->cacheTime = $cacheTime;
        if(Cache::has($this->getCacheKey($request)))
        {
            return Response(Cache::get($this->getCacheKey($request)));
        }
        return $next($request);
    }

    public function terminate(Request $request, Response $response)
    {
        return Cache::remember($this->getCacheKey($request), $this->cacheTime, function() use($response) {
            return $response->getContent();
        });
    }

    private function getCacheKey($request)
    {
        return md5($request->fullUrl().'_'.auth()->id());
    }
}
