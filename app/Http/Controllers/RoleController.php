<?php

namespace App\Http\Controllers;

use App\Http\Requests\roles\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Services\PermissionService;
use App\Services\RoleService;

class RoleController extends Controller
{
    protected $roleService;
    protected $permissionService;

    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
    }

    public function index()
    {
        $roles = $this->roleService->paginate();
        return view('roles.index', compact('roles'));
    }

    public function show($id)
    {
        $role = $this->roleService->findOrFail($id);
        return view('roles.show', compact('role'));
    }

    public function store(StoreRoleRequest $request)
    {
        $this->roleService->create($request);
        return redirect(route('roles.index'))->with('message', 'create'.$request->name.'success');
    }

    public function create()
    {
        $permissions = $this->permissionService->getFields(['id', 'name']);
        return view('roles.create', compact('permissions'));
    }

    public function update(UpdateRoleRequest $request, $id)
    {
        $this->roleService->update($request, $id);
        return redirect(route('roles.index'))->with('message', 'update'.$request->name.'success');
    }

    public function edit($id)
    {
        $permissions = $this->permissionService->getFields(['id', 'name']);
        $role = $this->roleService->findOrFail($id);
        return view('roles.edit', compact('role', 'permissions'));
    }

    public function destroy($id)
    {
        $this->roleService->delete($id);
        return redirect(route('roles.index'))->with('message', 'delete success');
    }
}
