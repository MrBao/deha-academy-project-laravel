<?php

namespace App\Http\Controllers;

use App\Http\Requests\products\StoreProductRequest;
use App\Http\Requests\products\UpdateProductRequest;
use App\Http\Resources\products\ProductResource;
use App\Services\ProductService;
use Illuminate\Contracts\Cache\Repository as Cache;

class ProductController extends Controller
{
    protected $productService;
    protected $cache;

    public function __construct(ProductService $productService, Cache $cache)
    {
        $this->productService = $productService;
        $this->cache = $cache;
    }

    public function index()
    {
        return view('products.index');
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $this->productService->update($request, $id);
        $product = $this->productService->find($id);
        $productResource = new ProductResource($product);
        return $this->getResourceResponse($productResource);
    }

    public function store(StoreProductRequest $request)
    {
        $product = $this->productService->create($request);
        $productResource = new ProductResource($product);
        return $this->getResourceResponse($productResource);
    }

    public function destroy($id)
    {
        $product = $this->productService->find($id);
        $this->productService->delete($id);
        $productResource = new ProductResource($product);
        return $this->getResourceResponse($productResource);
    }
}
