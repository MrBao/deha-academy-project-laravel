<?php

namespace App\Http\Controllers;

use App\Http\Requests\categories\StoreCategoryRequest;
use App\Http\Requests\categories\UpdateCategoryRequest;
use App\Services\CategoryService;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $categories = $this->categoryService->paginate();
        return view('categories.index', compact('categories'));
    }

    public function show($id)
    {
        $category = $this->categoryService->findOrFail($id);
        return view('categories.show', compact('category'));
    }

    public function store(StoreCategoryRequest $request)
    {
        $this->categoryService->create($request);
        return redirect(route('categories.index'))->with('message', 'create'.$request->name.'success');
    }

    public function create()
    {
        return view('categories.create');
    }

    public function edit($id)
    {
        $category = $this->categoryService->findOrFail($id);
        return view('categories.edit', compact('category'));
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        $this->categoryService->update($request, $id);
        return redirect(route('categories.index'))->with('message', 'update'.$request->name.'success');
    }

    public function destroy($id)
    {
        $this->categoryService->delete($id);
        return redirect(route('categories.index'))->with('message', 'delete success');
    }
}
