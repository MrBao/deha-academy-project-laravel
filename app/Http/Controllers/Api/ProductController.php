<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\products\ProductCollection;
use App\Http\Resources\products\ProductResource;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function getList(Request $request)
    {
        $products = $this->productService->search($request);
        $productCollection = new ProductCollection($products);
        return $this->getResourceResponse($productCollection);
    }

    public function get($id)
    {
        $product= $this->productService->findOrFail($id);
        $productResource = new ProductResource($product);
        return $this->getResourceResponse($productResource);
    }
}
