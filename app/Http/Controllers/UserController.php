<?php

namespace App\Http\Controllers;

use App\Http\Requests\users\StoreUserRequest;
use App\Http\Requests\users\UpdateUserRequest;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;
    protected $roleService;

    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
    }

    public function index(Request $request)
    {
        $users = $this->userService->search($request);
        $roles = $this->roleService->getFields(['id', 'name']);
        return view('users.index', compact('users', 'roles'));
    }

    public function show($id)
    {
        $user = $this->userService->findOrFail($id);
        return view('users.show', compact('user'));
    }

    public function store(StoreUserRequest $request)
    {
        $this->userService->create($request);
        return redirect(route('users.index'))->with('message', 'create'.$request->name.'success');
    }

    public function create()
    {
        $roles = $this->roleService->getFields(['id', 'name']);
        return view('users.create', compact('roles'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $this->userService->update($request, $id);
        return redirect(route('users.index'))->with('message', 'update'.$request->name.'success');
    }

    public function edit($id)
    {
        $roles = $this->roleService->getFields(['id', 'name']);
        $user = $this->userService->findOrFail($id);
        return view('users.edit', compact('user', 'roles'));
    }

    public function destroy($id)
    {
        $this->userService->delete($id);
        return redirect(route('users.index'))->with('message', 'delete success');
    }
}
