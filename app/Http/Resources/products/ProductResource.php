<?php

namespace App\Http\Resources\products;

use App\Traits\CheckPermission;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category_id' => $this->category_id,
            'category' => $this->category,
            'price' => $this->price,
            'image' => $this->image,
            'path_of_image' => $this->path_of_image,
            'show' => route('api-products.get', $this->id),
            'update' => route('products.update', $this->id),
            'delete' => route('products.destroy', $this->id),
        ];
    }
}
