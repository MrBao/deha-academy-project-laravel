echo "Check Convention"
phpcs --standard=PSR2 app/Traits
phpcs --standard=PSR2 app/Models
phpcs --standard=PSR2 app/Services
phpcs --standard=PSR2 app/Repositories
phpcs --standard=PSR2 app/Http/Controllers
phpcs --standard=PSR1,PSR2 --exclude=PSR1.Methods.CamelCapsMethodName tests/Feature/Categories
phpcs --standard=PSR1,PSR2 --exclude=PSR1.Methods.CamelCapsMethodName tests/Feature/Products
phpcs --standard=PSR1,PSR2 --exclude=PSR1.Methods.CamelCapsMethodName tests/Feature/Roles
phpcs --standard=PSR1,PSR2 --exclude=PSR1.Methods.CamelCapsMethodName tests/Feature/Users

echo "Fix Convention"
phpcbf --standard=PSR2 app/Traits
phpcbf --standard=PSR2 app/Models
phpcbf --standard=PSR2 app/Services
phpcbf --standard=PSR2 app/Repositories
phpcbf --standard=PSR2 app/Http/Controllers
phpcbf --standard=PSR2 tests/Feature/Categories
phpcbf --standard=PSR2 tests/Feature/Products
phpcbf --standard=PSR2 tests/Feature/Roles
phpcbf --standard=PSR2 tests/Feature/Users
