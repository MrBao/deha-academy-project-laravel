@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            <div class="card mt-4">
                <div class="card-header p-3">
                    <h5 class="mb-0">Create Role</h5>
                </div>
                <div class="card-body p-3">
                    <form role="form" class="text-start" action="{{route('roles.store')}}", method="POST">
                        @csrf
                        <label class="form-label">Name</label>
                        <div class="input-group input-group-outline my-3">
                          <input type="text" class="form-control" name="name">
                        </div>
                        @if ($errors->has('name'))
                            <div class="alert alert-danger alert-dismissible text-white" role="alert">
                                <span class="text-sm">{{ $errors->first('name') }}</span>
                                <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <label class="form-label">Permission</label>
                        @foreach ($permissions as $permission)
                            <div class="input-group input-group-outline my-3">
                                <input type="checkbox" name="permissions[]" value="{{$permission->id}}">
                                <label class="mb-0 mx-2 mt-1">{{$permission->name}}</label>
                            </div>
                        @endforeach
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 mt-sm-0 mt-2">
                              <button class="btn bg-gradient-info w-100 mb-0 toast-btn" type="submit" data-target="infoToast">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
