@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            @hasPermission('admin|user', 'roles.create')
                <a href="{{route('roles.create')}}" class="btn bg-gradient-info toast-btn"data-target="infoToast">Create +</a>
            @endhasPermission
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize ps-3">Roles table</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        No</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Name</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = ($roles->currentPage() - 1) * $roles->perPage() + 1;
                                @endphp
                                @foreach ($roles as $role)
                                <tr>
                                    <td>
                                        <div class="d-flex px-2 py-1 justify-content-center">
                                            <h6 class="mb-0 text-sm">{{$no++}}</h6>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-xs font-weight-bold mb-0">
                                            <a @hasPermission('admin|user', 'roles.show') href="{{route('roles.show', $role->id)}}" @endhasPermission>
                                                {{$role->name}}
                                            </a>
                                        </p>
                                    </td>
                                    <td class="align-middle text-center">
                                        @hasPermission('admin|user', 'roles.edit')
                                            <a href="{{route('roles.edit', $role->id)}}" class="text-secondary font-weight-bold text-xs"
                                                data-toggle="tooltip" data-original-title="Edit user">
                                                Edit
                                            </a>
                                        @endhasPermission
                                        <span>|</span>
                                        @hasPermission('admin|user', 'roles.destroy')
                                            <form action="{{route('roles.destroy',$role->id)}}" method="POST" class="d-inline-block">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="text-secondary font-weight-bold text-xs border-0" style="background:none"
                                                data-toggle="tooltip" data-original-title="Edit user">Delete</button>
                                            </form>
                                        @endhasPermission
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{$roles->links()}}
        </div>
    </div>
@endsection
