@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            @hasPermission('admin|user', 'categories.create')
                <a href="{{route('categories.create')}}" class="btn bg-gradient-info toast-btn"data-target="infoToast">Create +</a>
            @endhasPermission
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize ps-3">Categories table</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        No</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Name</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = ($categories->currentPage() - 1) * $categories->perPage() + 1;
                                @endphp
                                @foreach ($categories as $category)
                                <tr>
                                    <td>
                                        <div class="d-flex px-2 py-1 justify-content-center">
                                            <h6 class="mb-0 text-sm">{{$no++}}</h6>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-xs font-weight-bold mb-0">
                                            <a @hasPermission('admin|user', 'categories.show') href="{{route('categories.show', $category->id)}}" @endhasPermission>
                                                {{$category->name}}
                                            </a>
                                        </p>
                                    </td>
                                    <td class="align-middle text-center">
                                        @hasPermission('admin|user', 'categories.edit')
                                            <a href="{{route('categories.edit', $category->id)}}" class="text-secondary font-weight-bold text-xs"
                                                data-toggle="tooltip" data-original-title="Edit category">
                                                Edit
                                            </a>
                                        @endhasPermission
                                        <span>|</span>
                                        @hasPermission('admin|user', 'categories.destroy')
                                            <form action="{{route('categories.destroy', $category->id)}}" method="POST" class="d-inline-block">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="text-secondary font-weight-bold text-xs border-0" style="background:none"
                                                data-toggle="tooltip" data-original-title="Delete category">Delete</button>
                                            </form>
                                        @endhasPermission
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{$categories->links()}}
        </div>
    </div>
@endsection
