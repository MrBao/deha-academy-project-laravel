<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-modal">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 mx-auto">
                    <div class="card mt-4">
                        <div class="card-header p-3">
                            <h5 class="mb-0">Show Product</h5>
                        </div>
                        <div class="card-body p-3 pb-0">
                            <label class="form-label">Name</label>
                            <div class="input-group input-group-outline my-3">
                                <input disabled type="text" class="form-control" value="{{$product->name}}">
                            </div>
                            <label class="form-label">Category</label>
                            <div class="input-group input-group-outline my-3">
                                <input disabled type="text" class="form-control" value="{{$product->category->name}}">
                            </div>
                            <label class="form-label">Price</label>
                            <div class="input-group input-group-outline mt-3">
                            <input id="price" type="text" class="form-control" name="price" value="{{$product->price}}">
                            </div>
                            <label class="form-label">Image</label>
                            <div class="input-group input-group-outline mt-3">
                                <img src="{{$product->path_of_image}}" alt="image of this product">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        </div>
    </div>
    </div>
</div>
