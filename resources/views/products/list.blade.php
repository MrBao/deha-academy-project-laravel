<div class="card my-4">
    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
            <h6 class="text-white text-capitalize ps-3">Products table</h6>
        </div>
    </div>
    <div class="card-body px-0 pb-2">
        <div class="table-responsive p-0">
            <table class="table align-items-center mb-0">
                <thead>
                    <tr>
                        <th
                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                            No</th>
                        <th
                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                            Name</th>
                        <th
                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                            Category</th>
                        <th
                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                            Price</th>
                        <th
                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                            Image</th>
                        <th
                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                            Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>
                                <div class="d-flex px-2 py-1 justify-content-center">
                                    <h6 class="mb-0 text-sm">{{$loop->iteration}}</h6>
                                </div>
                            </td>
                            <td class="text-center">
                                <p class="text-xs font-weight-bold mb-0">
                                    <a @hasPermission('admin|user', 'products.show') data-api="{{route('api-products.get', $product->id)}}" type="button" class="show-product" @endhasPermission data-type="show">
                                        {{$product->name}}
                                    </a>
                                </p>
                            </td>
                            <td class="text-center">
                                <p class="text-xs font-weight-bold mb-0">
                                    {{$product->category->name}}
                                </p>
                            </td>
                            <td class="text-center">
                                <p class="text-xs font-weight-bold mb-0">
                                    {{$product->price}}
                                </p>
                            </td>
                            <td class="text-center">
                                <p class="text-xs font-weight-bold mb-0">
                                    {{$product->image}}
                                </p>
                            </td>
                            <td class="align-middle text-center">
                                @hasPermission('admin|user', 'products.edit')
                                    <a data-api="{{route('api-products.get', $product->id)}}" data-action="{{route('products.update', $product->id)}}" data-type="edit" class="text-secondary font-weight-bold text-xs edit-product" type="button">
                                        Edit
                                    </a>
                                @endhasPermission
                                <span>|</span>
                                @hasPermission('admin|user', 'products.destroy')
                                    <button type="button" class="text-secondary font-weight-bold text-xs border-0 btn-delete" style="background:none"
                                    data-toggle="tooltip" data-original-title="Delete" data-action="{{route('products.destroy', $product->id)}}">Delete</button>
                                @endhasPermission
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{{$products->links()}}
