<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-modal">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 mx-auto">
                    <div class="card mt-4">
                        <div class="card-header p-3">
                            <h5 class="mb-0">Update Product</h5>
                        </div>
                        <div class="card-body p-3">
                            <form role="form" class="text-start" method="POST" id="formData" enctype="multipart/form-data" action="{{ route('products.update', $product->id) }}">
                                <label class="form-label">Name</label>
                                <div class="input-group input-group-outline mt-3">
                                  <input id="name" type="text" class="form-control" name="name" value="{{$product->name}}">
                                </div>
                                <span class="text-sm text-danger d-block name"></span>
                                <label class="form-label">Category</label>
                                <div class="input-group input-group-outline mt-3">
                                    <select name="category_id" class="form-control" id="exampleFormControlSelect1">
                                        @foreach ($categories as $category)
                                            <option value="{{$category->id}}" @if($product->category->id == $category->id) selected @endif>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label class="form-label">Price</label>
                                <div class="input-group input-group-outline mt-3">
                                  <input id="price" type="text" class="form-control" name="price" value="{{$product->price}}">
                                </div>
                                <span class="text-sm text-danger d-block price"></span>
                                <label class="form-label">Image</label>
                                <div class="input-group input-group-outline mt-3">
                                    <img src="{{$product->path_of_image}}" alt="image of this product">
                                </div>
                                <div id="image-update">
                                    <label class="form-label">Update Image</label>
                                    <div class="input-group input-group-outline mt-3">
                                        <input id="image" type="file" class="form-control" name="image">
                                        <input type="hidden" name="_method" value="PUT">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-sm-6 col-12 mt-sm-0 mt-2">
                                      <button id="btn-update" class="btn bg-gradient-info mb-0 toast-btn" type="button" data-target="infoToast">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        </div>
    </div>
    </div>
</div>

