@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <a type="button" class="btn bg-gradient-info toast-btn" @unlesshasPermission('admin', 'products.create') invisible @endhasPermission data-target="infoToast" id="create-product" data-action="{{route('products.store')}}" data-type="create">Create +</a>
                <form role="form" class="text-start" action="{{route('api-products.list')}}" id="formSearch">
                    <input class="search-product" type="text" name="name" placeholder="Name" data-link="{{ route('products.index') }}">
                    <input class="search-product" type="text" name="price" placeholder="Price" data-link="{{ route('products.index') }}">
                    <input class="search-product" type="text" name="category" placeholder="Category" data-link="{{ route('products.index') }}">
                </form>
            </div>
            <div>
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Products table</h6>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            No</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Name</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Category</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Price</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Image</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Action</th>
                                    </tr>
                                </thead>
                                <tbody id="data-product">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="pagination">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="product-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-modal">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 mx-auto">
                            <div class="card mt-4">
                                <div class="card-header p-3">
                                    <h5 class="mb-0" id="modal-title"></h5>
                                </div>
                                <div class="card-body p-3">
                                    <form role="form" class="text-start" method="POST" id="formData" enctype="multipart/form-data">
                                        <label class="form-label">Name</label>
                                        <div class="input-group input-group-outline mt-3 mb-0">
                                            <input id="name" type="text" class="form-control" name="name">
                                        </div>
                                        <span class="text-sm text-danger d-block name"></span>
                                        <label class="form-label">Category</label>
                                        <div class="input-group input-group-outline mt-3 mb-0">
                                            <select name="category_id" class="form-control" id="categories" data-api="{{route('api-categories.all')}}">
                                            </select>
                                        </div>
                                        <span class="text-sm text-danger d-block category_id"></span>
                                        <label class="form-label">Price</label>
                                        <div class="input-group input-group-outline mt-3 mb-0">
                                            <input id="price" type="text" class="form-control" name="price">
                                        </div>
                                        <span class="text-sm text-danger d-block price"></span>
                                        <div id="show-image">
                                        </div>
                                        <div id="add-image">
                                        </div>
                                        <span class="text-sm text-danger d-block image"></span>
                                        <div class="row" id="product-submit">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
<script type="module" src="{{asset('assets/js/products/product.js')}}"></script>
@endsection
