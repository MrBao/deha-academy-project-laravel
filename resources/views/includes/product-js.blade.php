<script>
    function getListProducts (keyword = '', url = $('#formSearch').attr('action'))
    {
        let dataSearch = new FormData();
        dataSearch.append("keyword", keyword);
        callAjax(url, dataSearch, 'POST')
        .then(function (response) {
            $('#data-product').html(response.data);
            let links = $('nav>ul.pagination a');
            handlePaginate(links);
        });
    }

    function handlePaginate(selectors)
    {
        selectors.attr('type', 'button');
        selectors.each(function(){
            $(this).attr('data-link', $(this).attr('href'));
            $(this).removeAttr('href');
        });
    }

    function showMessage(message)
    {
        $('div#info-toast span').html(message);
        $('div#info-toast').removeClass('hide').addClass('show');
    }

    const Product = (function () {
    let modules = {};

    modules.delete = function (selector) {
        callAjax(selector.data('link'), {}, 'DELETE').then(function (response) {
            showMessage(response.message);
            getListProducts();
        });
    };

    modules.update = function () {
        let url = $('#formData').attr('action');
        let data = new FormData(formData);
        callAjax(url, data, 'POST').then(function (response) {
            showMessage(response.message);
            $('#modal-product>div').modal('hide');
            getListProducts();
        }).catch(function (response) {
            if (response.status == 422)
            {
                let errors = response.responseJSON.errors;
                for (let key in errors)
                {
                    $(`span.${key}`).html(errors[key][0]);
                }
            }
        });
    }

    modules.create = function () {
        let url = $('#formData').attr('action');
        let data = new FormData(formData);
        callAjax(url, data, 'POST').then(function (response) {
            showMessage(response.message);
            $('#modal-product>div').modal('hide');
            getListProducts();
        }).catch(function (response) {
            if (response.status == 422)
            {
                let errors = response.responseJSON.errors;
                for (let key in errors)
                {
                    $(`span.${key}`).html(errors[key][0]);
                }
            }
        });
    }

    modules.search = function () {
        getListProducts($('#search-input').val());
    }

    modules.show = function (selector) {
        callAjax(selector.data('link'))
        .then(function (response) {
            $('#modal-product').html(response.data);
            $('#modal-product>div').modal('show');
        });
    }

    modules.hide = function () {
        $('#modal-product>div').modal('hide');
    }

    modules.getNextPage = function (selector) {
        getListProducts('', selector.data('link'));
    }

    return modules;
}(window.jQuery, window, document));

$("document").ready(function() {
    const timeDelay = 1000;
    $(document).on('click', '#btn-update', function () {
        Product.update();
    });

    $(document).on('click', '#btn-create', function () {
        Product.create();
    });

    $(document).on("click",".btn-delete", function(){
        let result = confirm("Want to delete?");
        if (result) {
            Product.delete($(this));
        }
    });

    $('#search-input').on('keyup', debounce(Product.search, timeDelay));

    $(document).on("click",".show-product, #create-product, .edit-product", function(){
        Product.show($(this));
    });

    $('#modal-product').on('click', '#close-modal', function () {
        Product.hide();
    });

    $(document).on('click', 'a.page-link', function () {
        Product.getNextPage($(this));
    });
});
</script>
