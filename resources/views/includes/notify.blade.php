@if (session('message'))
    <h1 class="text-primary">{{ session('message') }}</h1>
@endif
<div class="position-fixed bottom-1 end-1 z-index-2">
    <div class="toast fade hide p-2 mt-2 bg-gradient-info" role="alert" aria-live="assertive" id="message-modal" aria-atomic="true">
        <div class="toast-header bg-transparent border-0">
            <i class="material-icons text-white me-2">
                notifications
            </i>
          <span id="message" class="me-auto text-white font-weight-bold"></span>
        </div>
      </div>
</div>
