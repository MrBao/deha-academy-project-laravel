@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <a href="{{route('users.create')}}" class="btn bg-gradient-info toast-btn @unlesshasPermission('admin', 'users.create') invisible @endhasPermission"data-target="infoToast">Create +</a>
                <form role="form" class="text-start" action="{{route('users.index')}}", method="GET">
                    <label for="name-input">Name</label>
                    <input id="name-input" type="text" name="name">
                    <label for="role-input">Roles</label>
                    <select name="role_id" class="" id="role-input">
                        <option value=""></option>
                        @foreach ($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                    <button class="btn bg-gradient-info mb-0 toast-btn" type="submit" data-target="infoToast">Search</button>
                </form>
            </div>
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize ps-3">Users table</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        No</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Name</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Email</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Role</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = ($users->currentPage() - 1) * $users->perPage() + 1;
                                @endphp
                                @foreach ($users as $user)
                                <tr>
                                    <td>
                                        <div class="d-flex px-2 py-1 justify-content-center">
                                            <h6 class="mb-0 text-sm">{{$no++}}</h6>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-xs font-weight-bold mb-0">
                                            <a @hasPermission('admin', 'users.show') href="{{route('users.show', $user->id)}}" @endhasPermission>
                                                {{$user->name}}
                                            </a>
                                        </p>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-xs font-weight-bold mb-0">{{$user->email}}</p>
                                    </td>
                                    <td class="text-center">
                                        <p class="text-xs font-weight-bold mb-0">{{$user->role->name ?? ""}}</p>
                                    </td>
                                    <td class="align-middle text-center">
                                        @hasPermission('admin', 'users.edit')
                                            <a href="{{route('users.edit', $user->id)}}" class="text-secondary font-weight-bold text-xs"
                                                data-toggle="tooltip" data-original-title="Edit user">
                                                Edit
                                            </a>
                                        @endhasPermission
                                        <span>|</span>
                                        @hasPermission('admin', 'users.destroy')
                                            <form action="{{route('users.destroy', $user->id)}}" method="POST" class="d-inline-block">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="text-secondary font-weight-bold text-xs border-0" style="background:none"
                                                data-toggle="tooltip" data-original-title="Delete user">Delete</button>
                                            </form>
                                        @endhasPermission
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{$users->links()}}
        </div>
    </div>
@endsection
