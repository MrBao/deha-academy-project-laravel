@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            <div class="card mt-4">
                <div class="card-header p-3">
                    <h5 class="mb-0">Update User</h5>
                </div>
                <div class="card-body p-3">
                    <form role="form" class="text-start" action="{{route('users.update', $user->id)}}", method="POST">
                        @csrf
                        @method('put')
                        <label class="form-label">Name</label>
                        <div class="input-group input-group-outline my-3">
                          <input type="text" class="form-control" name="name" value="{{$user->name}}">
                        </div>
                        @if ($errors->has('name'))
                            <div class="alert alert-danger alert-dismissible text-white" role="alert">
                                <span class="text-sm">{{ $errors->first('name') }}</span>
                                <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <label class="form-label">Email</label>
                        <div class="input-group input-group-outline my-3">
                          <input type="email" class="form-control" name="email" value="{{$user->email}}">
                        </div>
                        @if ($errors->has('email'))
                            <div class="alert alert-danger alert-dismissible text-white" role="alert">
                                <span class="text-sm">{{ $errors->first('email') }}</span>
                                <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <label class="form-label">Password</label>
                        <div class="input-group input-group-outline my-3">
                          <input type="password" class="form-control" name="password">
                        </div>
                        @if ($errors->has('password'))
                            <div class="alert alert-danger alert-dismissible text-white" role="alert">
                                <span class="text-sm">{{ $errors->first('password') }}</span>
                                <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <label class="form-label">Confirm password</label>
                        <div class="input-group input-group-outline my-3">
                          <input type="password" class="form-control" name="confirm-password">
                        </div>
                        @if ($errors->has('confirm-password'))
                            <div class="alert alert-danger alert-dismissible text-white" role="alert">
                                <span class="text-sm">{{ $errors->first('confirm-password') }}</span>
                                <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <label class="form-label">Role</label>
                        <div class="input-group input-group-outline my-3">
                            <select name="role_id" class="form-control" id="exampleFormControlSelect1">
                                <option value=""></option>
                                @foreach ($roles as $role)
                                    <option @if($role->id == $user->role_id) selected @endif value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 mt-sm-0 mt-2">
                              <button class="btn bg-gradient-info w-100 mb-0 toast-btn" type="submit" data-target="infoToast">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
