@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            <div class="card mt-4">
                <div class="card-header p-3">
                    <h5 class="mb-0">Show User</h5>
                </div>
                <div class="card-body p-3 pb-0">
                    <label class="form-label">Name</label>
                    <div class="input-group input-group-outline my-3">
                        <input disabled type="text" class="form-control" value="{{$user->name}}">
                    </div>
                    <label class="form-label">Email</label>
                    <div class="input-group input-group-outline my-3">
                        <input disabled type="text" class="form-control" value="{{$user->email}}">
                    </div>
                    <label class="form-label">Role</label>
                    <div class="input-group input-group-outline my-3">
                        <input disabled type="text" class="form-control" value="{{$user->role->name ?? ''}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
