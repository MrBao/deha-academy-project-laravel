module.exports = {
    "env": {
        "browser": true,
        "es2021": true,
        "es6": true,
        "jquery": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "rules": {
        "semi": "error",
        "indent": ["error", 4],
        "quotes": ["error", "double"],
        "no-console": "error",
        "no-unused-vars": "error",
        "comma-dangle": "error"
    }
};
