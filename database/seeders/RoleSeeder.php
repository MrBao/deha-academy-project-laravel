<?php

namespace Database\Seeders;

use App\Repositories\RoleRepository;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    public $roleRepository;
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function run()
    {
        $this->createSuperUserRole();
        $this->createAdminRole();
    }

    public function createSuperUserRole()
    {
        $roleSuperUser = [
            'name' => config('role.super_user')
        ];
        return $this->roleRepository->create($roleSuperUser);
    }

    public function createAdminRole()
    {
        $roleAdmin = [
            'name' => config('role.admin')
        ];
        return $this->roleRepository->create($roleAdmin);
    }
}
