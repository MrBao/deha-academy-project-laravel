<?php

namespace Database\Seeders;

use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    public $roleRepository;
    public $permissionRepository;
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function run()
    {
        $this->setAllPermissionToAdminRole();
    }

    public function setAllPermissionToAdminRole()
    {
        $role = $this->roleRepository->getAdminRole();
        $allPermissions = $this->permissionRepository->pluck('id')->toArray();
        $role->permission()->attach($allPermissions);
    }
}
