<?php

namespace Database\Seeders;

use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public $userRepository;
    public $roleRepository;
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    public function run()
    {
        $this->createSuperUser();
        $this->createAdminUser();
    }

    public function createSuperUser()
    {
        $superUserRole = $this->roleRepository->getSuperUserRole();
        $superUser = [
            'name' => 'SuperUser',
            'email' => 'super-user@deha-soft.com',
            'password' => Hash::make('123qweasd'),
            'role_id' => $superUserRole->id
        ];
        return $this->userRepository->create($superUser);
    }

    public function createAdminUser()
    {
        $adminRole = $this->roleRepository->getAdminRole();
        $admin = [
            'name' => 'admin',
            'email' => 'admin@deha-soft.com',
            'password' => Hash::make('123qweasd'),
            'role_id' => $adminRole->id
        ];
        return $this->userRepository->create($admin);
    }
}
