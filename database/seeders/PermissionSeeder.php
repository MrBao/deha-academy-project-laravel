<?php

namespace Database\Seeders;

use App\Repositories\PermissionRepository;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    public $permissionRepository;
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function run()
    {
        $this->createDefaultPermissions();
    }

    public function createDefaultPermissions()
    {
        $permissions = [
            'users.index',
            'users.show',
            'users.create',
            'users.edit',
            'users.destroy',
            'users.store',
            'users.update',
            'roles.index',
            'roles.show',
            'roles.create',
            'roles.edit',
            'roles.destroy',
            'roles.store',
            'roles.update',
            'products.index',
            'products.show',
            'products.create',
            'products.edit',
            'products.destroy',
            'products.store',
            'products.update',
            'categories.index',
            'categories.show',
            'categories.create',
            'categories.edit',
            'categories.destroy',
            'categories.store',
            'categories.update'
         ];

         foreach ($permissions as $permission) {
              $this->permissionRepository->create(['name' => $permission]);
         }
    }
}
