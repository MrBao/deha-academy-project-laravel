export const Base = (function () {
    let modules = {};

    modules.callAjax = function (url, data = {}, method="GET") {
        return $.ajax({
            method,
            url,
            data
        });
    };

    modules.callAjaxHasFile = function (url, data = {}, method="GET") {
        return $.ajax({
            method,
            url,
            data,
            processData: false,
            contentType: false,
            dataType: "json"
        });
    };

    modules.showMessage = function (message) {
        $("#message").html(message);
        $("#message-modal").removeClass("hide").addClass("show");
    };

    modules.renderErrors = function (errors) {
        for (let error in errors) {
            $(`.${error}`).html(errors[error][0]);
        }
    };

    modules.getDataAjaxPromise = function (url) {
        return new Promise(
            (resolve) =>
            {
                callAjaxApi(url, resolve);
            }
        );
    };
    return modules;
}(window.jQuery, window, document));

const callAjaxApi = (url, resolve) => {
    Base.callAjax(url).then(function (response) {
        resolve(response.data);
    });
};
