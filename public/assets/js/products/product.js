import { Base } from "../base/base.js";
import { Debounce } from "../debounce/debounce.js";

$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
    }
});

getList();

const timeDelay = 1000;
const failedValidation = 422;

const Product = (function () {
    let modules = {};

    modules.delete = function (selector) {
        Base.callAjax(selector.data("action"), {}, "DELETE").then(function (response) {
            Base.showMessage(response.message);
            getList();
        });
    };

    modules.createOrUpdate = function () {
        let formData = document.getElementById("formData");
        let url = $("#formData").attr("action");
        let data = new FormData(formData);
        Base.callAjaxHasFile(url, data, "POST").then(function (response) {
            Base.showMessage(response.message);
            modules.hideModal();
            getList();
        }).catch(function (response) {
            if (response.status == failedValidation) {
                Base.renderErrors(response.responseJSON.errors);
            }
        });
    };

    modules.showModal = async function (selector) {
        try {
            formReset();
            let type = selector.data("type");
            let product = await Base.getDataAjaxPromise(selector.data("api"));
            let categories = await Base.getDataAjaxPromise($("#categories").data("api"));
            fillProductModal(product, categories, type);
            $("#product-modal").modal("show");
        } catch (error) {
            alert(error);
        }
    };

    modules.hideModal = function () {
        $("#product-modal").modal("hide");
    };

    modules.getNextPage = function (selector) {
        let action = selector.attr("href");
        getListBy(action);
    };

    return modules;
}(window.jQuery, window, document));

$("document").ready(function () {
    $(document).on("click", "#btn-submit", function () {
        Product.createOrUpdate();
    });

    $(document).on("click",".btn-delete", function () {
        let result = confirm("Want to delete?");
        if (result) {
            Product.delete($(this));
        }
    });

    $(".search-product").on("keyup", Debounce.doFunction(getList, timeDelay));

    $(document).on("click",".show-product, #create-product, .edit-product", function () {
        Product.showModal($(this));
    });

    $(document).on("click", "#close-modal", function () {
        Product.hideModal();
    });

    $(document).on("click", "a.page-link", function (event) {
        event.preventDefault();
        Product.getNextPage($(this));
    });
});

function getListBy(url ="", dataSearch = "")
{
    Base.callAjax(url, dataSearch)
        .then(function (response) {
            $("#data-product").html(getListHtml(response.data));
        });
}

function getList()
{
    let formSearch = $("#formSearch");
    let url = formSearch.attr("action");
    let dataSearch = formSearch.serializeArray();
    getListBy(url, dataSearch);
}

function getListHtml(productsCollection)
{
    let products = productsCollection.data;
    let paginationHtml = productsCollection.pagination;
    let items = "";
    products.forEach(function (product, index) {
        items += `
            <tr>
                <td>
                    <div class="d-flex px-2 py-1 justify-content-center">
                        <h6 class="mb-0 text-sm">${index}</h6>
                    </div>
                </td>
                <td class="text-center">
                    <p class="text-xs font-weight-bold mb-0">
                        <a data-api="${product.show}" type="button" class="show-product" data-type="show">
                            ${product.name}
                        </a>
                    </p>
                </td>
                <td class="text-center">
                    <p class="text-xs font-weight-bold mb-0">
                        ${product.category.name}
                    </p>
                </td>
                <td class="text-center">
                    <p class="text-xs font-weight-bold mb-0">
                        ${product.price}
                    </p>
                </td>
                <td class="text-center">
                    <p class="text-xs font-weight-bold mb-0">
                        ${product.image}
                    </p>
                </td>
                <td class="align-middle text-center">
                    <a data-api="${product.show}" data-type="edit" class="text-secondary font-weight-bold text-xs edit-product" type="button">
                        Edit
                    </a>
                    <span>|</span>
                    <button type="button" class="text-secondary font-weight-bold text-xs border-0 btn-delete" style="background:none"
                    data-toggle="tooltip" data-original-title="Delete" data-action="${product.delete}">Delete</button>
                </td>
            </tr>
        `;
    });
    setPagination(paginationHtml);
    return items;
}

function setPagination(pagination)
{
    $("#pagination").html(pagination);
}

function fillProductModal(product, categories, type)
{
    switch (type) {
    case "show":
        fillModalShow(product, categories);
        break;
    case "create":
        fillModalCreate(product, categories);
        break;
    case "edit":
        fillModalEdit(product, categories);
        break;
    default:
        return false;
    }
}

function fillModalShow(product, categories)
{
    $("#modal-title").html("Show");
    $("#show-image").html(imageModalHtml);
    for (let field in product) {
        $(`#formData input[name=${field}]`).val(product[field]).attr("disabled", true);
        if (field == "path_of_image") {
            $("#image-src").attr("src", `${product[field]}?random=${getRandomInt(100)}`);
        }
    }
    $("#categories").html(getCategoriesHtml(categories, product)).attr("disabled", true);
}

function fillModalCreate(product, categories)
{
    $("#modal-title").html("Create");
    $("#add-image").html(createImageModalHtml);
    $("#product-submit").html(submitModalHtml);
    $("#categories").html(getCategoriesHtml(categories, product));
}

function fillModalEdit(product, categories)
{
    $("#formData").attr("action", product.update);
    $("#modal-title").html("Edit");
    $("#show-image").html(imageModalHtml);
    $("#add-image").html(updateImageModalHtml);
    $("#product-submit").html(submitModalHtml);
    for (let field in product) {
        $(`#formData input[name=${field}]`).not("input[name=image]").val(product[field]);
        if (field == "path_of_image") {
            $("#image-src").attr("src", `${product[field]}?random=${getRandomInt(100)}`);
        }
    }
    $("#categories").html(getCategoriesHtml(categories, product));
}

function getCategoriesHtml(categories, product)
{
    let categoryItems = "<option value=\"\">Choose Category</option>";
    for (let category of categories) {
        categoryItems += `<option value="${category.id}"${product ? `${product.category.id == category.id ? "selected" : ""}` : ""}>${category.name}</option>`;
    }
    return categoryItems;
}

function formReset()
{
    $("#formData input, select").attr("disabled", false);
    $("#formData").trigger("reset");
    $("#modal-title, #show-image, #add-image, #product-submit, span.text-danger").html("");
    $("#formData").attr("action", $("#create-product").data("action"));
}

function getRandomInt(max)
{
    return Math.floor(Math.random() * max);
}

const imageLabelHtml = "<label class=\"form-label\">Image</label>";
const inputFileHtml =   `<div class="input-group input-group-outline mt-3">
                            <input type="file" class="form-control" name="image">
                        </div>`;

const imageModalHtml =
    imageLabelHtml +
    `<div class="input-group input-group-outline mt-3">
        <img src="" alt="image of this product" id="image-src">
    </div>`;

const createImageModalHtml = imageLabelHtml + inputFileHtml;

const updateImageModalHtml = `
    <label class="form-label">Update Image</label>
    `+inputFileHtml+`
    <input id="method" type="hidden" name="_method" value="PUT">`;

const submitModalHtml = `
    <div class="col-lg-3 col-sm-6 col-12 mt-sm-0 mt-2">
        <button id="btn-submit" class="btn bg-gradient-info mb-0 toast-btn" type="button" data-target="infoToast">Submit</button>
    </div>`;
