export const Debounce = (function () {
    let modules = {};
    modules.doFunction = function (func, wait) {
        var timeout;
        return function() {
            var executeFunction = function() {
                func.apply();
            };

            clearTimeout(timeout);
            timeout = setTimeout(executeFunction, wait);
        };
    };
    return modules;
}(window.jQuery, window, document));
