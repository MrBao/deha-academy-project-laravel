## Setup (has file init.sh)

cp .env.example .env
composer install
php artisan key:generate
php artisan migrate
php artisan db:seed
php artisan storage:link


## PSR2 check (has file php-convention.sh)


## eslint check convention js (jas file js-convention.sh)
