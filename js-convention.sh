echo "Check Convention"

eslint public/assets/js/products
eslint public/assets/js/base

echo "Fix Convention"

eslint public/assets/js/products --fix
eslint public/assets/js/base --fix
