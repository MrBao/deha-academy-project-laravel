<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function () {
    Route::get('/', function () {
        return view('dashboard');
    });

    Route::controller(RoleController::class)->group(function (){
        Route::prefix('roles')->group(function (){
            Route::as('roles.')->group(function (){
                Route::get('/edit/{id}', 'edit')->name('edit')->middleware('hasPermission:admin,roles.edit');
                Route::get('/create', 'create')->name('create')->middleware('hasPermission:admin,roles.create');
                Route::get('/', 'index')->name('index')->middleware('hasPermission:admin,roles.index');
                Route::get('/{id}', 'show')->name('show')->middleware('hasPermission:admin,roles.show');
                Route::post('/', 'store')->name('store')->middleware('hasPermission:admin,roles.store');
                Route::put('/{id}', 'update')->name('update')->middleware('hasPermission:admin,roles.update');
                Route::delete('/{id}', 'destroy')->name('destroy')->middleware('hasPermission:admin,roles.destroy');
            });
        });
    });

    Route::controller(UserController::class)->group(function (){
        Route::prefix('users')->group(function (){
            Route::as('users.')->group(function (){
                Route::get('/create', 'create')->name('create')->middleware('hasPermission:admin,users.create');
                Route::get('/', 'index')->name('index')->middleware('hasPermission:admin,users.index', 'cache.response:100');
                Route::get('/{id}', 'show')->name('show')->middleware('hasPermission:admin,users.show', 'cache.response:5');
                Route::post('/', 'store')->name('store')->middleware('hasPermission:admin,users.store');
                Route::put('/{id}', 'update')->name('update')->middleware('hasPermission:admin,users.update');
                Route::get('/edit/{id}', 'edit')->name('edit')->middleware('hasPermission:admin,users.edit');
                Route::delete('/{id}', 'destroy')->name('destroy')->middleware('hasPermission:admin,users.destroy');
            });
        });
    });

    Route::controller(CategoryController::class)->group(function (){
        Route::prefix('categories')->group(function (){
            Route::as('categories.')->group(function (){
                Route::get('/all', 'getAll')->name('all')->middleware('hasPermission:admin,categories.index');
                Route::get('/create', 'create')->name('create')->middleware('hasPermission:admin,categories.create');
                Route::get('/', 'index')->name('index')->middleware('hasPermission:admin,categories.index');
                Route::get('/{id}', 'show')->name('show')->middleware('hasPermission:admin,categories.show');
                Route::post('/', 'store')->name('store')->middleware('hasPermission:admin,categories.store');
                Route::put('/{id}', 'update')->name('update')->middleware('hasPermission:admin,categories.update');
                Route::get('/edit/{id}', 'edit')->name('edit')->middleware('hasPermission:admin,categories.edit');
                Route::delete('/{id}', 'destroy')->name('destroy')->middleware('hasPermission:admin,categories.destroy');
            });
        });
    });

    Route::controller(ProductController::class)->group(function (){
        Route::prefix('products')->group(function (){
            Route::as('products.')->group(function (){
                Route::get('/create', 'create')->name('create')->middleware('hasPermission:admin,products.create');
                Route::get('/', 'index')->name('index')->middleware('hasPermission:admin,products.index');
                Route::get('/{id}', 'show')->name('show')->middleware('hasPermission:admin,products.show');
                Route::put('/{id}', 'update')->name('update')->middleware('hasPermission:admin,products.update');
                Route::get('/edit/{id}', 'edit')->name('edit')->middleware('hasPermission:admin,products.edit');
                Route::post('/', 'store')->name('store')->middleware('hasPermission:admin,products.store');
                Route::delete('/{id}', 'destroy')->name('destroy')->middleware('hasPermission:admin,products.destroy');
            });
        });
    });
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
