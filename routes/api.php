<?php

use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(ProductController::class)->group(function (){
    Route::prefix('products')->group(function (){
        Route::as('api-products.')->group(function (){
            Route::get('/list', 'getList')->name('list');
            Route::get('/{id}', 'get')->name('get');
        });
    });
});

Route::controller(CategoryController::class)->group(function (){
    Route::prefix('categories')->group(function (){
        Route::as('api-categories.')->group(function (){
            Route::get('/', 'getAll')->name('all');
        });
    });
});
